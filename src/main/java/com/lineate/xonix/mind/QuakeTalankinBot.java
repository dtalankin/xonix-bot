package com.lineate.xonix.mind;

import com.lineate.xonix.mind.model.Bot;
import com.lineate.xonix.mind.model.Cell;
import com.lineate.xonix.mind.model.GameStateView;
import com.lineate.xonix.mind.model.Move;

import java.util.Random;

public class QuakeTalankinBot implements Bot {

    String name;

    Random random;

    public QuakeTalankinBot() {
        this("Talankin.Quake");
    }

    public QuakeTalankinBot(String name) {
        this(name, new Random(1000));
    }

    public QuakeTalankinBot(String name, Random random) {
        this.name = name;
        this.random = random;
    }

    public String getName() {
        return name;
    }

    public Move move(GameStateView gs) {
        return Move.values()[random.nextInt(4)];
    }
}